uc_adaptive_paypal
Bastlynn, shoodnc@gmail.com
Mitesh, developermitesh@gmail.com
~~~
Install:
Unzip file into your /sites/modules/all directory, or the correct directory for your installation.
Go to admin/store/settings/payment/edit/methods to activate this payment method
Follow the directions to configure the API credentials in the Website Payments Pro settings.
    You do not need to have a Website Payments Pro account to set the settings.
    
Test *thoroughly* in the Sandbox environment before activating this or your own code based on it.
~~~
Release History:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
03.26.13 - v0.3 beta

1) chained payment configuarions (site owner to MP sellers)
2) Resolved most of the errors on checkout page.
3) Redirected to paypal with seller information.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
02.27.10 - v0.2 beta
Integration with MP is complete
Site commission is integrated with MP

Planned Developement Path:
1) Possibly chained payment configuarions (site owner to MP sellers)
2) If possible, send buyer shiping to PayPal to avoid having to enter data twice
3) Resolve conflict with Adaptive API with more than 5 recieving accounts
4) Check throughout for missing configurations to gracefully handle errors
5) Preconfigure CActions to push orders through correctly on confirmation

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
02.24.10 - v0.1 beta
Initial Release

This is a module *very* early in development for Paypal's Adaptive API. 
Please do not use this code without some very through testing on your own install.

Planned Development Path:
1) Integrate with marketplace module
2) Integrate site commission for each transaction to avoid monthly payout